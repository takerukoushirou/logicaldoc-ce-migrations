-- Unofficial LogicalDOC CE 8.2.1 to 8.3.4 database schema migration script by
-- Michael Maier <michael.maier@xplo.re>

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
-- IN THE SOFTWARE.


ALTER TABLE `ld_attributeset_ext`
  CHANGE COLUMN `ld_doublevalue` `ld_doublevalue` double DEFAULT NULL,
  ADD COLUMN `ld_hidden` int(11) NOT NULL AFTER `ld_setid`,
  ADD COLUMN `ld_multiple` int(11) NOT NULL AFTER `ld_hidden`,
  ADD COLUMN `ld_parent` varchar(255) DEFAULT NULL AFTER `ld_multiple`,
  ADD COLUMN `ld_stringvalues` varchar(4000) DEFAULT NULL AFTER `ld_stringvalue`;

ALTER TABLE `ld_document`
  ADD COLUMN `ld_links` int(11) NOT NULL AFTER `ld_formid`;

ALTER TABLE `ld_document_ext`
  CHANGE COLUMN `ld_doublevalue` `ld_doublevalue` double DEFAULT NULL,
  ADD COLUMN `ld_hidden` int(11) NOT NULL AFTER `ld_setid`,
  ADD COLUMN `ld_multiple` int(11) NOT NULL AFTER `ld_hidden`,
  ADD COLUMN `ld_parent` varchar(255) DEFAULT NULL AFTER `ld_multiple`,
  ADD COLUMN `ld_stringvalues` varchar(4000) DEFAULT NULL AFTER `ld_stringvalue`;

DROP TABLE `ld_feedmessage`;

ALTER TABLE `ld_folder`
  ADD COLUMN `ld_path` varchar(4000) DEFAULT NULL;

ALTER TABLE `ld_folder_ext`
  CHANGE COLUMN `ld_doublevalue` `ld_doublevalue` double DEFAULT NULL,
  ADD COLUMN `ld_hidden` int(11) NOT NULL AFTER `ld_setid`,
  ADD COLUMN `ld_multiple` int(11) NOT NULL AFTER `ld_hidden`,
  ADD COLUMN `ld_parent` varchar(255) DEFAULT NULL AFTER `ld_multiple`,
  ADD COLUMN `ld_stringvalues` varchar(4000) DEFAULT NULL AFTER `ld_stringvalue`;

ALTER TABLE `ld_folder_history`
  CHANGE COLUMN `ld_folderid` `ld_folderid` bigint(20) DEFAULT NULL;

ALTER TABLE `ld_generic`
  CHANGE COLUMN `ld_double1` `ld_double1` double DEFAULT NULL,
  CHANGE COLUMN `ld_double2` `ld_double2` double DEFAULT NULL;

ALTER TABLE `ld_generic_ext`
  CHANGE COLUMN `ld_doublevalue` `ld_doublevalue` double DEFAULT NULL,
  ADD COLUMN `ld_hidden` int(11) NOT NULL AFTER `ld_setid`,
  ADD COLUMN `ld_multiple` int(11) NOT NULL AFTER `ld_hidden`,
  ADD COLUMN `ld_parent` varchar(255) DEFAULT NULL AFTER `ld_multiple`,
  ADD COLUMN `ld_stringvalues` varchar(4000) DEFAULT NULL AFTER `ld_stringvalue`;

ALTER TABLE `ld_group`
  ADD COLUMN `ld_source` varchar(255) DEFAULT NULL AFTER `ld_type`;

ALTER TABLE `ld_history`
  CHANGE COLUMN `ld_folderid` `ld_folderid` bigint(20) DEFAULT NULL;

ALTER TABLE `ld_note`
  DROP COLUMN `ld_snippet`,
  ADD COLUMN `ld_filename` varchar(255) DEFAULT NULL AFTER `ld_date`,
  ADD COLUMN `ld_fileversion` varchar(10) DEFAULT NULL AFTER `ld_page`,
  ADD COLUMN `ld_opacity` int(11) NOT NULL AFTER `ld_fileversion`,
  ADD COLUMN `ld_color` varchar(255) DEFAULT NULL AFTER `ld_opacity`,
  ADD COLUMN `ld_left` double NOT NULL AFTER `ld_color`,
  ADD COLUMN `ld_top` double NOT NULL AFTER `ld_left`,
  ADD COLUMN `ld_width` double NOT NULL AFTER `ld_top`,
  ADD COLUMN `ld_height` double NOT NULL AFTER `ld_width`;

ALTER TABLE `ld_template_ext`
  CHANGE COLUMN `ld_doublevalue` `ld_doublevalue` double DEFAULT NULL,
  ADD COLUMN `ld_hidden` int(11) NOT NULL AFTER `ld_setid`,
  ADD COLUMN `ld_multiple` int(11) NOT NULL AFTER `ld_hidden`,
  ADD COLUMN `ld_parent` varchar(255) DEFAULT NULL AFTER `ld_multiple`,
  ADD COLUMN `ld_stringvalues` varchar(4000) DEFAULT NULL AFTER `ld_stringvalue`;

ALTER TABLE `ld_user`
  ADD COLUMN `ld_creation` datetime DEFAULT NULL AFTER `ld_key`;

ALTER TABLE `ld_user_history`
  ADD INDEX `LD_UHIST_UID` (`ld_userid`);

ALTER TABLE `ld_version`
  ADD COLUMN `ld_links` int(11) NOT NULL AFTER `ld_formid`;

ALTER TABLE `ld_version_ext`
  CHANGE COLUMN `ld_doublevalue` `ld_doublevalue` double DEFAULT NULL,
  ADD COLUMN `ld_hidden` int(11) NOT NULL AFTER `ld_setid`,
  ADD COLUMN `ld_multiple` int(11) NOT NULL AFTER `ld_hidden`,
  ADD COLUMN `ld_parent` varchar(255) DEFAULT NULL AFTER `ld_multiple`,
  ADD COLUMN `ld_stringvalues` varchar(4000) DEFAULT NULL AFTER `ld_stringvalue`;

CREATE TABLE `ld_dashlet` (
  `ld_id` bigint(20) NOT NULL,
  `ld_lastmodified` datetime NOT NULL,
  `ld_recordversion` bigint(20) NOT NULL,
  `ld_deleted` int(11) NOT NULL,
  `ld_tenantid` bigint(20) NOT NULL,
  `ld_name` varchar(255) DEFAULT NULL,
  `ld_title` varchar(255) DEFAULT NULL,
  `ld_type` varchar(255) DEFAULT NULL,
  `ld_query` varchar(4000) DEFAULT NULL,
  `ld_content` varchar(4000) DEFAULT NULL,
  `ld_max` int(11) DEFAULT NULL,
  PRIMARY KEY (`ld_id`),
  UNIQUE KEY `AK_DASHLET` (`ld_name`, `ld_tenantid`)
) ENGINE=InnoDB;


-- Remove hibernate sequence for dropped table.
DELETE FROM `hibernate_sequences` WHERE `sequence_name` = 'ld_feedmessage';

-- New menu items.
INSERT INTO `ld_menu` (`ld_id`, `ld_lastmodified`, `ld_recordversion`, `ld_deleted`, `ld_tenantid`, `ld_name`, `ld_parentid`, `ld_securityref`, `ld_icon`, `ld_type`, `ld_description`, `ld_position`)
VALUES
  (1526, NOW(), 1, 0, 1, 'calendar', 1520, NULL, 'menu.png', 1, NULL, 40),
  (1606, NOW(), 1, 0, 1, 'calendar', 1500, NULL, 'menu.png', 1, NULL, 10),
  (1607, NOW(), 1, 0, 1, 'signature', 1500, NULL, 'menu.png', 1, NULL, 10);

INSERT INTO `ld_menugroup` (`ld_menuid`, `ld_groupid`, `ld_write`)
VALUES
  (1526, -10000, 0),
  (1526, 2, 0),
  (1526, 3, 0),
  (1526, 4, 0),
  (1606, -10000, 0),
  (1606, 2, 0),
  (1606, 3, 0),
  (1606, 4, 0),
  (1607, -10000, 0),
  (1607, 2, 0),
  (1607, 3, 0),
  (1607, 4, 0);

-- Rename subtypes for default generic usersettings.
UPDATE `ld_generic` SET `ld_subtype` = "dashlet-notes" WHERE `ld_id` = -52;
UPDATE `ld_generic` SET `ld_subtype` = "dashlet-locked" WHERE `ld_id` = -51;
UPDATE `ld_generic` SET `ld_subtype` = "dashlet-checkout" WHERE `ld_id` = -50;

-- Update default message templates.
UPDATE `ld_messagetemplate`
  SET
    `ld_lastmodified` = NOW(),
    `ld_body` = '$I18N.format(\'emailnotifyaccount\', $user.fullName)<br/>\r\n$I18N.get(\'username\'): <b>$user.username</b><br/>\r\n$I18N.get(\'password\'): <b>$password</b><br/>\r\n$I18N.get(\'clickhere\'): <a href=\"$url\">$url</a><br/><br/>\r\n$I18N.get(\'askedtochangepswdatlogin\')'
  WHERE `ld_id` = 2;

UPDATE `ld_messagetemplate`
  SET
    `ld_lastmodified` = NOW(),
    `ld_body` = '$product - $I18N.get(\'passwordrequest\')<br/>\r\n$I18N.get(\'clickhere\'): <a href=\"$url\">$url</a>'
  WHERE `ld_id` = 3;

-- Default dashlets.
INSERT INTO `ld_dashlet` (`ld_id`, `ld_lastmodified`, `ld_recordversion`, `ld_deleted`, `ld_tenantid`, `ld_name`, `ld_title`, `ld_type`, `ld_query`, `ld_content`, `ld_max`)
VALUES
  (1, NOW(), 1, 0, 1, 'checkout', 'event.checkedoutdocs', 'document', 'from Document where lockUserId=$user.id and status=1 order by date desc', NULL, 10),
  (2, NOW(), 1, 0, 1, 'checkin', 'event.checkedindocs', 'docevent', 'from DocumentHistory where userId=$user.id and event=\'event.checkedin\' order by date desc', NULL, 10),
  (3, NOW(), 1, 0, 1, 'locked', 'event.lockeddocs', 'document', 'from Document where lockUserId=$user.id and status=2 order by date desc', NULL, 10),
  (4, NOW(), 1, 0, 1, 'download', 'event.downloadeddocs', 'docevent', 'from DocumentHistory where userId=$user.id and event=\'event.downloaded\' order by date desc', NULL, 10),
  (5, NOW(), 1, 0, 1, 'change', 'event.changeddocs', 'docevent', 'from DocumentHistory where userId=$user.id and event=\'event.changed\' order by date desc', NULL, 10),
  (6, NOW(), 1, 0, 1, 'notes', 'lastnotes', 'note', 'from DocumentNote where userId=$user.id order by date desc', NULL, 10),
  (7, NOW(), 1, 0, 1, 'tagcloud', 'tagcloud', 'content', NULL, NULL, 10);

-- Set filename and fileversion for notes based on latest document data. If the
-- fileversion column value is not set, affected notes will not be shown in the GUI.
UPDATE `ld_note`
  SET
    `ld_filename` = (
        SELECT `ld_document`.`ld_filename`
        FROM `ld_document`
        WHERE `ld_document`.`ld_id` = `ld_note`.`ld_docid`
      ),
    -- Also set defaults used by new notes.
    `ld_opacity` = 80,
    `ld_color` = "#FFFF88",
    `ld_left` = 0.5,
    `ld_top` = 0.5,
    `ld_width` = 0.15,
    `ld_height` = 0.1,
    `ld_shape` = "square",
    `ld_linecolor` = "#a1a1a1",
    `ld_lineopacity` = 80,
    `ld_linewidth` = 1
    WHERE `ld_filename` IS NULL;

UPDATE `ld_note`
  SET
    `ld_fileversion` = (
        SELECT `ld_document`.`ld_fileversion`
        FROM `ld_document`
        WHERE `ld_document`.`ld_id` = `ld_note`.`ld_docid`
      )
    WHERE `ld_fileversion` IS NULL;