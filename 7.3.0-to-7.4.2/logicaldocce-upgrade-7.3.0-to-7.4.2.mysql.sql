-- Unofficial LogicalDOC CE 7.3 to 7.4.2 database schema migration script by
-- Michael Maier <michael.maier@xplo.re>

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
-- IN THE SOFTWARE.


/*!40101 SET NAMES utf8 */;
/*!40101 SET @`OLD_SQL_MODE` = @@`SQL_MODE`, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @`OLD_SQL_NOTES` = @@`SQL_NOTES`, SQL_NOTES = 0 */;

ALTER TABLE `ld_document`
  CHANGE COLUMN `ld_version` `ld_version` varchar(255) NULL DEFAULT NULL,
  ADD COLUMN `ld_lockuser` varchar(255) DEFAULT NULL AFTER `ld_lockuserid`,
  MODIFY COLUMN `ld_stamped` int(11) NOT NULL AFTER `ld_signed`,
  CHANGE COLUMN `ld_nature` `ld_nature` int(11) NOT NULL,
  CHANGE COLUMN `ld_formid` `ld_formid` bigint(20) NULL DEFAULT NULL;

ALTER TABLE `ld_folder`
  CHANGE COLUMN `ld_hidden` `ld_hidden` int(11) NOT NULL;

ALTER TABLE `ld_folder_history`
  CHANGE COLUMN `ld_version` `ld_version` varchar(255) NULL DEFAULT NULL;

ALTER TABLE `ld_history`
  CHANGE COLUMN `ld_version` `ld_version` varchar(255) NULL DEFAULT NULL;

ALTER TABLE `ld_ticket`
  ADD COLUMN `ld_suffix` varchar(255) DEFAULT NULL;

ALTER TABLE `ld_user`
  ADD COLUMN `ld_emailsignature` varchar(4000) DEFAULT NULL AFTER `ld_email`,
  CHANGE COLUMN `ld_ipwhitelist` `ld_ipwhitelist` varchar(1000) DEFAULT NULL,
  CHANGE COLUMN `ld_ipblacklist` `ld_ipblacklist` varchar(1000) DEFAULT NULL;

ALTER TABLE `ld_version`
  CHANGE COLUMN `ld_version` `ld_version` varchar(255) DEFAULT NULL,
  ADD COLUMN `ld_lockuser` varchar(255) DEFAULT NULL AFTER `ld_lockuserid`,
  MODIFY COLUMN `ld_stamped` int(11) NOT NULL AFTER `ld_signed`,
  CHANGE COLUMN `ld_nature` `ld_nature` int(11) NOT NULL,
  CHANGE COLUMN `ld_formid` `ld_formid` bigint(20) DEFAULT NULL;

INSERT INTO `hibernate_unique_key` (`tablename`, `next_hi`)
VALUES
  ('ld_group_ext', 100);

/*!40111 SET SQL_NOTES = @`OLD_SQL_NOTES` */;
/*!40101 SET SQL_MODE = @`OLD_SQL_MODE` */;