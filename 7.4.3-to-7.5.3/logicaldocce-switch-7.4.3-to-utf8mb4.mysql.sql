-- Unofficial LogicalDOC CE 7.4.3 database schema migration script to switch to utf8mb4 by
-- Michael Maier <michael.maier@xplo.re>

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
-- IN THE SOFTWARE.


-- Alter defaults for the database itself for future migrations (which are
-- character-set agnostic).
-- ATTENTION: Update your database name.
ALTER DATABASE `logicaldoc_7_4_3` DEFAULT CHARACTER SET `utf8mb4` DEFAULT COLLATE `utf8mb4_unicode_ci`;

ALTER TABLE `hibernate_unique_key`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `tablename` `tablename` varchar(40) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL;

ALTER TABLE `ld_bookmark`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_title` `ld_title` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL,
  CHANGE COLUMN `ld_description` `ld_description` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_filetype` `ld_filetype` varchar(40) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_contact`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_firstname` `ld_firstname` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_lastname` `ld_lastname` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_email` `ld_email` varchar(512) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_company` `ld_company` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_address` `ld_address` varchar(512) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_phone` `ld_phone` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_mobile` `ld_mobile` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_document`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_customid` `ld_customid` varchar(200) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_title` `ld_title` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_version` `ld_version` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_fileversion` `ld_fileversion` varchar(10) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_publisher` `ld_publisher` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_creator` `ld_creator` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_type` `ld_type` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_lockuser` `ld_lockuser` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_source` `ld_source` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_sourceauthor` `ld_sourceauthor` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_sourceid` `ld_sourceid` varchar(1000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_sourcetype` `ld_sourcetype` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_object` `ld_object` varchar(1000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_coverage` `ld_coverage` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_language` `ld_language` varchar(10) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_filename` `ld_filename` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_digest` `ld_digest` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_recipient` `ld_recipient` varchar(1000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_exportname` `ld_exportname` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_exportversion` `ld_exportversion` varchar(10) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_docreftype` `ld_docreftype` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_comment` `ld_comment` varchar(1000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_workflowstatus` `ld_workflowstatus` varchar(1000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_transactionid` `ld_transactionid` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_extresid` `ld_extresid` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_tgs` `ld_tgs` varchar(1000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_document_ext`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_stringvalue` `ld_stringvalue` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL,
  CHANGE COLUMN `ld_label` `ld_label` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_extoption`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_attribute` `ld_attribute` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL,
  CHANGE COLUMN `ld_value` `ld_value` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL,
  CHANGE COLUMN `ld_label` `ld_label` varchar(1000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_feedmessage`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_guid` `ld_guid` varchar(512) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_title` `ld_title` varchar(512) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_description` `ld_description` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_link` `ld_link` varchar(512) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_folder`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_description` `ld_description` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_creator` `ld_creator` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_folder_ext`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_stringvalue` `ld_stringvalue` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL,
  CHANGE COLUMN `ld_label` `ld_label` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_folder_history`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_username` `ld_username` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_event` `ld_event` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_comment` `ld_comment` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_version` `ld_version` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_title` `ld_title` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_titleold` `ld_titleold` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_path` `ld_path` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_pathold` `ld_pathold` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_sessionid` `ld_sessionid` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_filename` `ld_filename` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_filenameold` `ld_filenameold` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_foldergroup`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`;

ALTER TABLE `ld_generic`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_type` `ld_type` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL,
  CHANGE COLUMN `ld_subtype` `ld_subtype` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL,
  CHANGE COLUMN `ld_string1` `ld_string1` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_string2` `ld_string2` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_string3` `ld_string3` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_generic_ext`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_stringvalue` `ld_stringvalue` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL,
  CHANGE COLUMN `ld_label` `ld_label` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_group`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL,
  CHANGE COLUMN `ld_description` `ld_description` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_history`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_username` `ld_username` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_event` `ld_event` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_comment` `ld_comment` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_version` `ld_version` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_title` `ld_title` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_titleold` `ld_titleold` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_path` `ld_path` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_pathold` `ld_pathold` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_sessionid` `ld_sessionid` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_filename` `ld_filename` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_filenameold` `ld_filenameold` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_link`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_type` `ld_type` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL;

ALTER TABLE `ld_menu`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_text` `ld_text` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_icon` `ld_icon` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_description` `ld_description` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_menugroup`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`;

ALTER TABLE `ld_messagetemplate`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL,
  CHANGE COLUMN `ld_language` `ld_language` varchar(10) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL,
  CHANGE COLUMN `ld_description` `ld_description` varchar(1000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_body` `ld_body` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_subject` `ld_subject` varchar(1000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_note`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_username` `ld_username` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_message` `ld_message` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_snippet` `ld_snippet` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_rating`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`;

ALTER TABLE `ld_recipient`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL,
  CHANGE COLUMN `ld_address` `ld_address` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL,
  CHANGE COLUMN `ld_mode` `ld_mode` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL;

ALTER TABLE `ld_sequence`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL;

ALTER TABLE `ld_systemmessage`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_author` `ld_author` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_messagetext` `ld_messagetext` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_subject` `ld_subject` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_tag`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_tag` `ld_tag` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_temp`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_string` `ld_string` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_template`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL,
  CHANGE COLUMN `ld_description` `ld_description` varchar(2000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_template_ext`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_stringvalue` `ld_stringvalue` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL,
  CHANGE COLUMN `ld_label` `ld_label` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_tenant`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_displayname` `ld_displayname` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_street` `ld_street` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_postalcode` `ld_postalcode` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_city` `ld_city` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_country` `ld_country` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_state` `ld_state` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_email` `ld_email` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_telephone` `ld_telephone` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_ticket`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_ticketid` `ld_ticketid` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL,
  CHANGE COLUMN `ld_suffix` `ld_suffix` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_user`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_cert` `ld_cert` varchar(4000) CHARACTER SET `ascii` COLLATE `ascii_bin` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_certsubject` `ld_certsubject` varchar(255) CHARACTER SET `utf8` COLLATE `utf8_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_certdigest` `ld_certdigest` varchar(255) CHARACTER SET `ascii` COLLATE `ascii_bin` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_key` `ld_key` varchar(4000) CHARACTER SET `ascii` COLLATE `ascii_bin` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_keydigest` `ld_keydigest` varchar(255) CHARACTER SET `ascii` COLLATE `ascii_bin` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_ipwhitelist` `ld_ipwhitelist` varchar(1000) CHARACTER SET `ascii` COLLATE `ascii_bin` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_ipblacklist` `ld_ipblacklist` varchar(1000) CHARACTER SET `ascii` COLLATE `ascii_bin` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_password` `ld_password` varchar(255) CHARACTER SET `ascii` COLLATE `ascii_bin` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_passwordmd4` `ld_passwordmd4` varchar(255) CHARACTER SET `ascii` COLLATE `ascii_bin` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_username` `ld_username` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_firstname` `ld_firstname` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_street` `ld_street` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_postalcode` `ld_postalcode` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_city` `ld_city` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_country` `ld_country` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_state` `ld_state` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_language` `ld_language` varchar(10) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_email` `ld_email` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_emailsignature` `ld_emailsignature` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_telephone` `ld_telephone` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_telephone2` `ld_telephone2` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_user_history`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_username` `ld_username` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_event` `ld_event` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_comment` `ld_comment` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_sessionid` `ld_sessionid` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_filename` `ld_filename` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_usergroup`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`;

ALTER TABLE `ld_version`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_customid` `ld_customid` varchar(200) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_title` `ld_title` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_version` `ld_version` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_fileversion` `ld_fileversion` varchar(10) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_publisher` `ld_publisher` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_creator` `ld_creator` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_type` `ld_type` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_lockuser` `ld_lockuser` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_source` `ld_source` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_sourceauthor` `ld_sourceauthor` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_sourceid` `ld_sourceid` varchar(1000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_sourcetype` `ld_sourcetype` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_object` `ld_object` varchar(1000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_coverage` `ld_coverage` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_language` `ld_language` varchar(10) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_filename` `ld_filename` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_digest` `ld_digest` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_recipient` `ld_recipient` varchar(1000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_foldername` `ld_foldername` varchar(1000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_templatename` `ld_templatename` varchar(1000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_tgs` `ld_tgs` varchar(1000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_username` `ld_username` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_comment` `ld_comment` varchar(1000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_event` `ld_event` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_exportname` `ld_exportname` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_exportversion` `ld_exportversion` varchar(10) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_workflowstatus` `ld_workflowstatus` varchar(1000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_transactionid` `ld_transactionid` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_extresid` `ld_extresid` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;

ALTER TABLE `ld_version_ext`
  CHARACTER SET = `utf8mb4`,
  COLLATE = `utf8mb4_unicode_ci`,
  CHANGE COLUMN `ld_stringvalue` `ld_stringvalue` varchar(4000) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NOT NULL,
  CHANGE COLUMN `ld_label` `ld_label` varchar(255) CHARACTER SET `utf8mb4` COLLATE `utf8mb4_unicode_ci` NULL DEFAULT NULL;