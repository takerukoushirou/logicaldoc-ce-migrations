-- Unofficial LogicalDOC CE 7.4.3 to 7.5.3 database schema migration script by
-- Michael Maier <michael.maier@xplo.re>

-- ATTENTION!
-- This script does not migrate any of the following extended document attributes
-- that have been moved into a separate table (check usage first!):
-- Coverage, Object, Recipient, Source, Source Author, Source Date,
-- Source Original ID, Source Type

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
-- IN THE SOFTWARE.


CREATE TABLE `ld_attributeset` (
  `ld_id` bigint(20) NOT NULL,
  `ld_lastmodified` datetime NOT NULL,
  `ld_recordversion` bigint(20) NOT NULL,
  `ld_deleted` int(11) NOT NULL,
  `ld_tenantid` bigint(20) NOT NULL,
  `ld_name` varchar(255) NOT NULL,
  `ld_description` varchar(2000) DEFAULT NULL,
  `ld_readonly` int(11) NOT NULL,
  `ld_type` int(11) NOT NULL,
  PRIMARY KEY (`ld_id`),
  UNIQUE KEY `AK_ATTRIBUTESET` (`ld_name`, `ld_tenantid`)
) ENGINE = InnoDB;

INSERT INTO `hibernate_unique_key` (`tablename`, `next_hi`)
VALUES
  ('ld_attributeset', 101);

INSERT INTO `ld_attributeset` (`ld_id`, `ld_lastmodified`, `ld_recordversion`, `ld_deleted`, `ld_tenantid`, `ld_name`, `ld_description`, `ld_readonly`, `ld_type`)
VALUES
  (-1, NOW(), 1, 0, 1, 'default', 'default', 1, 0);

CREATE TABLE `ld_attributeset_ext` (
  `ld_attsetid` bigint(20) NOT NULL,
  `ld_mandatory` int(11) NOT NULL,
  `ld_type` int(11) NOT NULL,
  `ld_editor` int(11) NOT NULL,
  `ld_position` int(11) NOT NULL,
  `ld_stringvalue` varchar(4000) COLLATE `utf8mb4_unicode_ci` DEFAULT NULL,
  `ld_intvalue` bigint(20) DEFAULT NULL,
  `ld_doublevalue` float DEFAULT NULL,
  `ld_datevalue` datetime DEFAULT NULL,
  `ld_name` varchar(255) COLLATE `utf8mb4_unicode_ci` NOT NULL,
  `ld_label` varchar(255) COLLATE `utf8mb4_unicode_ci` DEFAULT NULL,
  `ld_setid` bigint(20) NOT NULL,
  PRIMARY KEY (`ld_setid`, `ld_name`),
  KEY `FK_ATT_ATTSET` (`ld_attsetid`),
  CONSTRAINT `FK_ATT_ATTSET` FOREIGN KEY (`ld_attsetid`) REFERENCES `ld_attributeset` (`ld_id`)
) ENGINE = InnoDB;

INSERT INTO `ld_attributeset_ext` (`ld_attsetid`, `ld_mandatory`, `ld_type`, `ld_editor`, `ld_position`, `ld_stringvalue`, `ld_intvalue`, `ld_doublevalue`, `ld_datevalue`, `ld_name`, `ld_label`, `ld_setid`)
VALUES
  (-1, 0, 0, 0, 5, NULL, NULL, NULL, NULL, 'coverage', 'Coverage', -1),
  (-1, 0, 0, 0, 4, NULL, NULL, NULL, NULL, 'object', 'Object', -1),
  (-1, 0, 0, 0, 6, NULL, NULL, NULL, NULL, 'recipient', 'Recipient', -1),
  (-1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 'source', 'Source', -1),
  (-1, 0, 0, 0, 1, NULL, NULL, NULL, NULL, 'sourceAuthor', 'Author', -1),
  (-1, 0, 3, 0, 7, NULL, NULL, NULL, NULL, 'sourceDate', 'Date', -1),
  (-1, 0, 0, 0, 2, NULL, NULL, NULL, NULL, 'sourceId', 'Original ID', -1),
  (-1, 0, 0, 0, 3, NULL, NULL, NULL, NULL, 'sourceType', 'Type', -1);

ALTER TABLE `ld_document`
  DROP COLUMN `ld_sourcedate`,
  DROP COLUMN `ld_sourcetype`;

ALTER TABLE `ld_document_ext`
  ADD COLUMN `ld_setid` bigint(20) NULL AFTER `ld_label`,
  ADD INDEX `DOC_EXT_NAME` (`ld_name`);

ALTER TABLE `ld_extoption`
  CHANGE COLUMN `ld_templateid` `ld_setid` bigint(20) NOT NULL;

ALTER TABLE `ld_folder`
  MODIFY COLUMN `ld_hidden` int(11) NOT NULL AFTER `ld_quotasize`,
  ADD COLUMN `ld_foldref` bigint(20) DEFAULT NULL,
  ADD COLUMN `ld_level` int(11) DEFAULT NULL;

ALTER TABLE `ld_folder_ext`
  ADD COLUMN `ld_setid` bigint(20) NULL AFTER `ld_label`;

ALTER TABLE `ld_foldergroup`
  DROP PRIMARY KEY,
  ADD PRIMARY KEY (`ld_folderid`, `ld_groupid`),
  ADD COLUMN `ld_print` int(11) NOT NULL AFTER `ld_subscription`;

ALTER TABLE `ld_generic_ext`
  ADD COLUMN `ld_setid` bigint(20) NULL AFTER `ld_label`;

ALTER TABLE `ld_menugroup`
  DROP PRIMARY KEY,
  ADD PRIMARY KEY (`ld_menuid`, `ld_groupid`),
  DROP FOREIGN KEY `FKB4F7F679AA456AD1`,
  ADD CONSTRAINT `FK_MGROUP_MENU` FOREIGN KEY (`ld_menuid`) REFERENCES `ld_menu` (`ld_id`) ON DELETE CASCADE;

ALTER TABLE `ld_template`
  DROP COLUMN `ld_category`,
  DROP COLUMN `ld_signrequired`;

ALTER TABLE `ld_template_ext`
  ADD COLUMN `ld_setid` bigint(20) NULL AFTER `ld_label`;

INSERT IGNORE INTO `ld_template` (`ld_id`, `ld_lastmodified`, `ld_recordversion`, `ld_deleted`, `ld_tenantid`, `ld_name`, `ld_description`, `ld_readonly`, `ld_type`) VALUES
  (-1, NOW(), 1, 0, 1, 'default', 'default', 0, 0);

INSERT IGNORE INTO `ld_template_ext` (`ld_templateid`, `ld_mandatory`, `ld_type`, `ld_editor`, `ld_position`, `ld_stringvalue`, `ld_intvalue`, `ld_doublevalue`, `ld_datevalue`, `ld_name`, `ld_label`, `ld_setid`) VALUES
  (-1, 0, 0, 0, 5, NULL, NULL, NULL, NULL, 'coverage', 'Coverage', -1),
  (-1, 0, 0, 0, 4, NULL, NULL, NULL, NULL, 'object', 'Object', -1),
  (-1, 0, 0, 0, 6, NULL, NULL, NULL, NULL, 'recipient', 'Recipient', -1),
  (-1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 'source', 'Source', -1),
  (-1, 0, 0, 0, 1, NULL, NULL, NULL, NULL, 'sourceAuthor', 'Author', -1),
  (-1, 0, 3, 0, 7, NULL, NULL, NULL, NULL, 'sourceDate', 'Date', -1),
  (-1, 0, 0, 0, 2, NULL, NULL, NULL, NULL, 'sourceId', 'Original ID', -1),
  (-1, 0, 0, 0, 3, NULL, NULL, NULL, NULL, 'sourceType', 'Type', -1);

CREATE TABLE `ld_uniquetag` (
  `ld_tag` varchar(255) NOT NULL,
  `ld_tenantid` bigint(20) NOT NULL,
  `ld_count` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ld_tag`, `ld_tenantid`)
) ENGINE = InnoDB;

CREATE TABLE `ld_update` (
  `ld_update` varchar(255) DEFAULT NULL,
  `ld_date` datetime DEFAULT NULL,
  `ld_version` varchar(255) DEFAULT NULL
) ENGINE = InnoDB;

ALTER TABLE `ld_version`
  DROP COLUMN `ld_sourcedate`,
  DROP COLUMN `ld_sourcetype`;

ALTER TABLE `ld_version_ext`
  ADD COLUMN `ld_setid` bigint(20) NULL AFTER `ld_label`;

DELETE FROM `hibernate_unique_key` WHERE `tablename` = 'ld_group_ext';

-- Extension to existing schema to ensure that no duplicates exist.
ALTER TABLE `hibernate_unique_key`
  ADD PRIMARY KEY (`tablename`);

-- New menu entry.
INSERT INTO `ld_menu` (`ld_id`, `ld_lastmodified`, `ld_recordversion`, `ld_deleted`, `ld_tenantid`, `ld_text`, `ld_parentid`, `ld_securityref`, `ld_icon`, `ld_type`, `ld_description`, `ld_position`)
VALUES
  (1605, NOW(), 1, 0, 1, 'aliases', 1500, NULL, 'menu.png', 1, NULL, 10);