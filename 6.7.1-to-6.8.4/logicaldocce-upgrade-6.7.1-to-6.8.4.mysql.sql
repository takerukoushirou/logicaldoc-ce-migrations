-- Unofficial LogicalDOC CE 6.7.1 to 6.8.4 database schema migration script by
-- Michael Maier <michael.maier@xplo.re>

-- NOTES
-- (1) See below for menu groups associations in ld_menu table.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
-- IN THE SOFTWARE.


/*!40101 SET NAMES utf8 */;
/*!40101 SET @`OLD_SQL_MODE` = @@`SQL_MODE`, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @`OLD_SQL_NOTES` = @@`SQL_NOTES`, SQL_NOTES = 0 */;

CREATE TABLE `ld_contact` (
  `ld_id` bigint NOT NULL,
  `ld_lastmodified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ld_deleted` int NOT NULL,
  `ld_userid` bigint DEFAULT NULL,
  `ld_firstname` varchar(255) DEFAULT NULL,
  `ld_lastname` varchar(255) DEFAULT NULL,
  `ld_email` varchar(512) DEFAULT NULL,
  `ld_company` varchar(255) DEFAULT NULL,
  `ld_address` varchar(512) DEFAULT NULL,
  `ld_phone` varchar(255) DEFAULT NULL,
  `ld_mobile` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ld_id`)
) ENGINE = InnoDB;

ALTER TABLE `ld_document`
  CHANGE COLUMN `ld_customid` `ld_customid` varchar(200) NULL DEFAULT NULL;

ALTER TABLE `ld_folder`
  ADD COLUMN `ld_deleteuserid` bigint NULL DEFAULT NULL AFTER `ld_templocked`;

ALTER TABLE `ld_user`
  ADD COLUMN `ld_passwordmd4` varchar(255) NULL DEFAULT NULL AFTER `ld_password`;

ALTER TABLE `ld_version`
  CHANGE COLUMN `ld_customid` `ld_customid` varchar(200) NULL DEFAULT NULL;

-- The following new column statement is also contained in the first 7.0.2RC2 upgrade script.
ALTER TABLE `ld_systemmessage`
  ADD COLUMN `ld_html` int NOT NULL DEFAULT '0' AFTER `ld_type`;

-- The following new index statement is also contained in the second 7.0.2RC2 upgrade script.
ALTER TABLE `ld_tag`
  ADD INDEX `LD_TAG_TAG` (`ld_tag`);

ALTER TABLE `ld_ticket`
  DROP INDEX `FK_TICKET_DOC`;

ALTER TABLE `ld_version`
  DROP INDEX `FK_VERSION_USER`;

DROP TABLE IF EXISTS `ld_group_ext`;
DROP TABLE IF EXISTS `ld_workflowhistory`;

ALTER TABLE `ld_userdoc`
  DROP FOREIGN KEY `FK_USERDOC_USER`,
  ADD CONSTRAINT `FK_USERDOC_USER` FOREIGN KEY (`ld_userid`) REFERENCES `ld_user` (`ld_id`);

ALTER TABLE `ld_usergroup`
  ADD CONSTRAINT `FK2435438D76F11EA1` FOREIGN KEY (`ld_groupid`) REFERENCES `ld_group` (`ld_id`);

ALTER TABLE `ld_menu`
  DROP FOREIGN KEY `FK_MENU_PARENT`,
  ADD CONSTRAINT `FK_MENU_PARENT` FOREIGN KEY (`ld_parentid`) REFERENCES `ld_menu` (`ld_id`);

-- You most likely also want to add menu groups associations for the new menu
-- 1530 (like the ones existing for menu 5).
INSERT INTO `ld_menu` (`ld_id`, `ld_lastmodified`, `ld_recordversion`, `ld_deleted`, `ld_tenantid`, `ld_text`, `ld_parentid`, `ld_securityref`, `ld_icon`, `ld_type`, `ld_description`)
VALUES
  (1530, NOW(), 1, 0, 1, 'contacts', 5, NULL, 'menu.png', 1, NULL);

/*!40111 SET SQL_NOTES = @`OLD_SQL_NOTES` */;
/*!40101 SET SQL_MODE = @`OLD_SQL_MODE` */;