-- Unofficial LogicalDOC CE 7.6.4 to 7.7.6 database schema migration script by
-- Michael Maier <michael.maier@xplo.re>

-- ATTENTION!
-- User certificates will be deleted as a new certificate storage system was
-- introduced and no migration of existing certificate data is performed.
-- As document titles have been removed from version 7.7.6 and only the filename
-- is being used, all filenames will be updated based on their current title.
-- Later versions automatically updated filenames, but earlier documents may
-- still have their original filenames that differ from their title.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
-- IN THE SOFTWARE.


RENAME TABLE `hibernate_unique_key` TO `hibernate_sequences`;

ALTER TABLE `hibernate_sequences`
  DROP PRIMARY KEY,
  CHANGE COLUMN `tablename` `sequence_name` varchar(40) NOT NULL,
  CHANGE COLUMN `next_hi` `sequence_next_hi_value` bigint(20) NOT NULL,
  ADD PRIMARY KEY (`sequence_name`);

INSERT INTO `hibernate_sequences` (`sequence_name`, `sequence_next_hi_value`)
VALUES
  ('ld_session', 100);

-- Update document filenames to match title with extension.
UPDATE `ld_document` SET `ld_filename` = CONCAT(`ld_title`, ".", `ld_type`);

ALTER TABLE `ld_document`
  DROP COLUMN `ld_title`,
  ADD COLUMN `ld_workflowstatusdisp` varchar(1000) NULL AFTER `ld_workflowstatus`;

ALTER TABLE `ld_folder_history`
  DROP COLUMN `ld_title`,
  DROP COLUMN `ld_titleold`,
  ADD COLUMN `ld_ip` varchar(255) NULL AFTER `ld_filenameold`;

ALTER TABLE `ld_foldergroup`
  ADD COLUMN `ld_move` int(11) NOT NULL,
  ADD COLUMN `ld_email` int(11) NOT NULL;

-- Update document filenames to match title with extension.
UPDATE `ld_history` SET `ld_filename` = CONCAT(`ld_title`, ".", SUBSTRING_INDEX(`ld_filename`, '.', -1));

ALTER TABLE `ld_history`
  DROP COLUMN `ld_title`,
  DROP COLUMN `ld_titleold`,
  ADD COLUMN `ld_ip` varchar(255) NULL AFTER `ld_userlogin`;

INSERT INTO `ld_menu` (`ld_id`, `ld_lastmodified`, `ld_recordversion`, `ld_deleted`, `ld_tenantid`, `ld_name`, `ld_parentid`, `ld_securityref`, `ld_icon`, `ld_type`, `ld_description`, `ld_position`)
VALUES
  (1535, NOW(), 1, 0, 1, 'interfacedensity', 110, NULL, 'menu.png', 1, NULL, 5);

INSERT INTO `ld_menugroup` (`ld_menuid`, `ld_groupid`, `ld_write`)
VALUES
  (1535, -10000, 0),
  (1535, 2, 0),
  (1535, 3, 0),
  (1535, 4, 0);

CREATE TABLE `ld_session` (
  `ld_id` bigint(20) NOT NULL,
  `ld_lastmodified` datetime NOT NULL,
  `ld_recordversion` bigint(20) NOT NULL,
  `ld_deleted` int(11) NOT NULL,
  `ld_tenantid` bigint(20) NOT NULL,
  `ld_sid` varchar(255) NOT NULL,
  `ld_username` varchar(255) DEFAULT NULL,
  `ld_key` varchar(255) DEFAULT NULL,
  `ld_node` varchar(255) DEFAULT NULL,
  `ld_tenantname` varchar(255) DEFAULT NULL,
  `ld_creation` datetime DEFAULT NULL,
  `ld_lastrenew` datetime DEFAULT NULL,
  `ld_status` int(11) NOT NULL,
  `ld_clientid` varchar(255) DEFAULT NULL,
  `ld_clientaddr` varchar(255) DEFAULT NULL,
  `ld_clienthost` varchar(255) DEFAULT NULL,
  UNIQUE KEY `AK_SESSION` (`ld_sid`)
) ENGINE = InnoDB;

ALTER TABLE `ld_user`
  DROP COLUMN `ld_cert`,
  DROP COLUMN `ld_certsubject`,
  DROP COLUMN `ld_certdigest`,
  DROP COLUMN `ld_key`,
  DROP COLUMN `ld_keydigest`,
  ADD COLUMN `ld_certexpire` datetime NULL,
  ADD COLUMN `ld_certdn` varchar(1000) NULL,
  ADD COLUMN `ld_certpass` varchar(255) NULL,
  ADD COLUMN `ld_secondfactor` varchar(255) NULL,
  ADD COLUMN `ld_key` varchar(255);

-- Update document filenames to match title with extension.
UPDATE `ld_version` SET `ld_filename` = CONCAT(`ld_title`, ".", `ld_type`);

ALTER TABLE `ld_version`
  DROP COLUMN `ld_title`,
  ADD COLUMN `ld_workflowstatusdisp` varchar(1000) NULL AFTER `ld_workflowstatus`;