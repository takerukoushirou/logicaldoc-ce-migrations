-- Unofficial LogicalDOC CE 8.3.4 to 8.4.2 database schema migration script by
-- Michael Maier <michael.maier@xplo.re>

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
-- IN THE SOFTWARE.


ALTER TABLE `ld_document`
  ADD COLUMN `ld_ocrtemplateid` bigint(20) DEFAULT NULL AFTER `ld_links`,
  ADD COLUMN `ld_ocrd` int(11) NOT NULL AFTER `ld_ocrtemplateid`,
  ADD COLUMN `ld_barcodetemplateid` bigint(20) DEFAULT NULL AFTER `ld_ocrd`;

ALTER TABLE `ld_folder`
  CHANGE COLUMN `ld_path` `ld_path` varchar(255) DEFAULT NULL,
  ADD COLUMN `ld_grid` varchar(4000) DEFAULT NULL AFTER `ld_path`,
  ADD COLUMN `ld_ocrtemplateid` bigint(20) DEFAULT NULL AFTER `ld_grid`,
  ADD COLUMN `ld_barcodetemplateid` bigint(20) DEFAULT NULL AFTER `ld_ocrtemplateid`,
  ADD INDEX `LD_FLD_FOLDREF` (`ld_foldref`),
  ADD INDEX `LD_FLD_PATH` (`ld_path`);

ALTER TABLE `ld_ticket`
  ADD COLUMN `ld_enabled` int(11) NOT NULL AFTER `ld_suffix`,
  ADD COLUMN `ld_maxcount` int(11) DEFAULT NULL AFTER `ld_enabled`;

ALTER TABLE `ld_user`
  ADD COLUMN `ld_docsgrid` text DEFAULT NULL AFTER `ld_key`,        -- actually varchar(4000), but leads to too large row size
  ADD COLUMN `ld_hitsgrid` text DEFAULT NULL AFTER `ld_docsgrid`;   -- actually varchar(4000), but leads to too large row size

ALTER TABLE `ld_version`
  ADD COLUMN `ld_ocrtemplateid` bigint(20) DEFAULT NULL AFTER `ld_links`,
  ADD COLUMN `ld_ocrd` int(11) NOT NULL AFTER `ld_ocrtemplateid`,
  ADD COLUMN `ld_barcodetemplateid` bigint(20) DEFAULT NULL AFTER `ld_ocrd`;


-- New menu items.
INSERT INTO `ld_menu` (`ld_id`, `ld_lastmodified`, `ld_recordversion`, `ld_deleted`, `ld_tenantid`, `ld_name`, `ld_parentid`, `ld_securityref`, `ld_icon`, `ld_type`, `ld_description`, `ld_position`)
VALUES
  (-7, NOW(), 1, 0, 1, 'downloadtickets', 90, NULL, 'menu.png', 1, NULL, 1),
  (10, NOW(), 1, 0, 1, 'index', 1500, NULL, 'menu.png', 1, NULL, 1),
  (11, NOW(), 1, 0, 1, 'userinterface', 1500, NULL, 'menu.png', 1, NULL, 1),
  (1608, NOW(), 1, 0, 1, 'ocr', 1500, NULL, 'menu.png', 1, NULL, 10);

INSERT INTO `ld_menugroup` (`ld_menuid`, `ld_groupid`, `ld_write`)
VALUES
  (10, -10000, 0),
  (10, 2, 0),
  (10, 3, 0),
  (10, 4, 0),
  (11, -10000, 0),
  (11, 2, 0),
  (11, 3, 0),
  (11, 4, 0);