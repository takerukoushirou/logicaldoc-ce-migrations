-- Unofficial LogicalDOC CE 7.5.3 to 7.6.4 database schema migration script by
-- Michael Maier <michael.maier@xplo.re>

-- ATTENTION!
-- This script will remove the data columns for the following extended document
-- attributes that have been moved into a separate table (check usage first!):
-- Coverage, Object, Recipient, Source, Source Author, Source Date,
-- Source Original ID, Source Type

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
-- IN THE SOFTWARE.


ALTER TABLE `ld_document`
  DROP COLUMN `ld_coverage`,
  DROP COLUMN `ld_object`,
  DROP COLUMN `ld_recipient`,
  DROP COLUMN `ld_source`,
  DROP COLUMN `ld_sourceauthor`,
  DROP COLUMN `ld_sourceid`,
  ADD COLUMN `ld_password` varchar(255) NULL AFTER `ld_filename`;

ALTER TABLE `ld_document_ext`
  DROP INDEX `DOC_EXT_NAME`,
  ADD INDEX `LD_EXT_NAME` (`ld_name`);

ALTER TABLE `ld_folder`
  ADD COLUMN `ld_storage` int(11) NULL,
  ADD COLUMN `ld_maxversions` int(11) NULL,
  ADD COLUMN `ld_color` varchar(255) NULL,
  ADD COLUMN `ld_tgs` varchar(1000) NULL,
  ADD COLUMN `ld_qthreshold` int(11) NULL,
  ADD COLUMN `ld_qrecipients` varchar(1000) NULL;

ALTER TABLE `ld_folder_history`
  ADD COLUMN `ld_userlogin` varchar(255) NULL AFTER `ld_filename`;

ALTER TABLE `ld_foldergroup`
  ADD COLUMN `ld_password` int(11) NOT NULL;

CREATE TABLE `ld_foldertag` (
  `ld_folderid` bigint(20) NOT NULL,
  `ld_tenantid` bigint(20) NOT NULL,
  `ld_tag` varchar(255) DEFAULT NULL,
  KEY `FK_TAG_FOLDER` (`ld_folderid`),
  KEY `LD_FTAG_TAG` (`ld_tag`),
  CONSTRAINT `FK_TAG_FOLDER` FOREIGN KEY (`ld_folderid`) REFERENCES `ld_folder` (`ld_id`)
) ENGINE = InnoDB;

ALTER TABLE `ld_history`
  ADD COLUMN `ld_userlogin` varchar(255) NULL AFTER `ld_filenameold`;

ALTER TABLE `ld_menu`
  CHANGE COLUMN `ld_text` `ld_name` varchar(255) NULL;

INSERT INTO `ld_menu` (`ld_id`, `ld_lastmodified`, `ld_recordversion`, `ld_deleted`, `ld_tenantid`, `ld_name`, `ld_parentid`, `ld_securityref`, `ld_icon`, `ld_type`, `ld_description`, `ld_position`)
VALUES
  (-6, NOW(), 1, 0, 1, 'deletedfolders', 90, NULL, 'menu.png', 1, NULL, 1),
  (70, NOW(), 1, 0, 1, 'general', 80, NULL, 'menu.png', 1, NULL, 20),
  (71, NOW(), 1, 0, 1, 'sessions', 70, NULL, 'menu.png', 1, NULL, 20),
  (72, NOW(), 1, 0, 1, 'logs', 70, NULL, 'menu.png', 1, NULL, 20),
  (73, NOW(), 1, 0, 1, 'runlevel', 70, NULL, 'menu.png', 1, NULL, 20),
  (105, NOW(), 1, 0, 1, 'repositories', 7, NULL, 'repositories.png', 1, NULL, 60),
  (200, NOW(), 1, 0, 1, 'textcontent', 16, NULL, 'text.png', 1, NULL, 1),
  (1601, NOW(), 1, 0, 1, 'sessions', 1600, NULL, 'menu.png', 1, NULL, 10),
  (1602, NOW(), 1, 0, 1, 'trash', 1500, NULL, 'menu.png', 1, NULL, 10);

INSERT INTO `ld_menugroup` (`ld_menuid`, `ld_groupid`, `ld_write`)
VALUES
  (200, 2, 0),
  (200, 3, 0),
  (200, 4, 0),
  (1602, -10000, 0),
  (1602, 2, 0),
  (1602, 3, 0),
  (1602, 4, 0);

ALTER TABLE `ld_messagetemplate`
  ADD COLUMN `ld_type` varchar(255) NULL AFTER `ld_body`;

INSERT INTO `ld_messagetemplate` (`ld_id`, `ld_lastmodified`, `ld_recordversion`, `ld_deleted`, `ld_tenantid`, `ld_name`, `ld_language`, `ld_description`, `ld_body`, `ld_type`, `ld_subject`)
VALUES
  (4, NOW(), 1, 0, 1, 'newdoc', 'en', NULL, '$I18N.get(''newdocscreated'')<br/>
<hr/>
$message
<hr/>
$I18N.get(''user''): <b>$creator.fullName</b><br/>
$I18N.get(''date''): <b>$DateTool.format($CURRENT_DATE, true)</b><br/>
<hr/>
<b>$I18N.get(''documents'')</b>:
#foreach( $doc in $documents )
  <br/>$doc.fileName | <a href="$DocTool.downloadUrl($doc)">$I18N.get(''download'')</a> | <a href="$DocTool.displayUrl($doc)">$I18N.get(''display'')</a>
#end', 'system', '$product - $I18N.get(''newdocscreated'')');

ALTER TABLE `ld_rating`
  ADD COLUMN `ld_username` varchar(255) NULL AFTER `ld_vote`;

ALTER TABLE `ld_tenant`
  ADD COLUMN `ld_qthreshold` int(11) NULL,
  ADD COLUMN `ld_qrecipients` varchar(1000) NULL;

ALTER TABLE `ld_user`
  CHANGE `ld_emailsignature` `ld_emailsignature` varchar(1000) DEFAULT NULL,
  ADD COLUMN `ld_defworkspace` bigint(20) DEFAULT NULL,
  ADD COLUMN `ld_email2` varchar(255) DEFAULT NULL,
  ADD COLUMN `ld_emailsignature2` varchar(1000) DEFAULT NULL;

ALTER TABLE `ld_user_history`
  ADD COLUMN `ld_userlogin` varchar(255) NULL,
  ADD COLUMN `ld_ip` varchar(255) NULL;

ALTER TABLE `ld_version`
  DROP COLUMN `ld_coverage`,
  DROP COLUMN `ld_object`,
  DROP COLUMN `ld_recipient`,
  DROP COLUMN `ld_source`,
  DROP COLUMN `ld_sourceauthor`,
  DROP COLUMN `ld_sourceid`,
  ADD COLUMN `ld_password` varchar(255) NULL AFTER `ld_filename`;