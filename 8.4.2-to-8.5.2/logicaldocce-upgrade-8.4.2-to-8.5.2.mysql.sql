-- Unofficial LogicalDOC CE 8.4.2 to 8.5.2 database schema migration script by
-- Michael Maier <michael.maier@xplo.re>

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
-- IN THE SOFTWARE.


ALTER TABLE `ld_foldergroup`
  ADD COLUMN `ld_storage` int(11) NOT NULL AFTER `ld_automation`;

ALTER TABLE `ld_generic`
  ADD COLUMN `ld_string5` varchar(1000) DEFAULT NULL AFTER `ld_string4`,
  ADD COLUMN `ld_string6` varchar(1000) DEFAULT NULL AFTER `ld_string5`,
  ADD COLUMN `ld_string7` varchar(1000) DEFAULT NULL AFTER `ld_string6`,
  ADD COLUMN `ld_string8` varchar(1000) DEFAULT NULL AFTER `ld_string7`;

ALTER TABLE `ld_note`
  ADD COLUMN `ld_type` varchar(255) DEFAULT NULL AFTER `ld_height`,
  ADD COLUMN `ld_recipient` varchar(255) DEFAULT NULL AFTER `ld_type`,
  ADD COLUMN `ld_recipientemail` varchar(255) DEFAULT NULL AFTER `ld_recipient`;

ALTER TABLE `ld_systemmessage`
  CHANGE COLUMN `ld_messagetext` `ld_messagetext` mediumtext DEFAULT NULL,
  CHANGE COLUMN `ld_subject` `ld_subject` varchar(1000) DEFAULT NULL;

ALTER TABLE `ld_user`
  DROP COLUMN `ld_certpass`;


-- New menu items and updates to existing.
INSERT INTO `ld_menu` (`ld_id`, `ld_lastmodified`, `ld_recordversion`, `ld_deleted`, `ld_tenantid`, `ld_name`, `ld_parentid`, `ld_securityref`, `ld_icon`, `ld_type`, `ld_description`, `ld_position`)
VALUES
  (12, NOW(), 1, 0, 1, 'dropspot', 1500, NULL, 'menu.png', 1, NULL, 1);

UPDATE `ld_menu` SET `ld_name` = "protocols" WHERE `ld_id` = 3;

INSERT INTO `ld_menugroup` (`ld_menuid`, `ld_groupid`, `ld_write`)
VALUES
  (12, -10000, 0),
  (12, 2, 0),
  (12, 3, 0),
  (12, 4, 0);

-- Add new default message template.
INSERT INTO `ld_messagetemplate` (`ld_id`, `ld_lastmodified`, `ld_recordversion`, `ld_deleted`, `ld_tenantid`, `ld_name`, `ld_language`, `ld_description`, `ld_body`, `ld_type`, `ld_subject`)
VALUES
  (5, '2021-08-26 11:47:04', 1, 0, 1, 'empty', 'en', NULL, NULL, 'user', NULL);