-- Unofficial LogicalDOC CE 7.1 to 7.2.1 database schema migration script by
-- Michael Maier <michael.maier@xplo.re>

-- ATTENTION!
-- Use with care. Drops user signatures, archives current menu and sets up a
-- fresh 7.2.1 menu structure.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
-- IN THE SOFTWARE.


/*!40101 SET NAMES utf8 */;
/*!40101 SET @`OLD_SQL_MODE` = @@`SQL_MODE`, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @`OLD_SQL_NOTES` = @@`SQL_NOTES`, SQL_NOTES = 0 */;

ALTER TABLE `ld_bookmark`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_recordversion` `ld_recordversion` bigint(20) NOT NULL AFTER `ld_lastmodified`,
  CHANGE COLUMN `ld_tenantid` `ld_tenantid` bigint(20) NOT NULL AFTER `ld_deleted`;

ALTER TABLE `ld_contact`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_recordversion` `ld_recordversion` bigint(20) NOT NULL AFTER `ld_lastmodified`,
  CHANGE COLUMN `ld_tenantid` `ld_tenantid` bigint(20) NOT NULL AFTER `ld_deleted`;

ALTER TABLE `ld_document`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_recordversion` `ld_recordversion` bigint(20) NOT NULL AFTER `ld_lastmodified`,
  CHANGE COLUMN `ld_tenantid` `ld_tenantid` bigint(20) NOT NULL AFTER `ld_deleted`,
  CHANGE COLUMN `ld_customid` `ld_customid` varchar(200) DEFAULT NULL,
  CHANGE COLUMN `ld_date` `ld_date` datetime DEFAULT NULL,
  CHANGE COLUMN `ld_creation` `ld_creation` datetime NOT NULL,
  CHANGE COLUMN `ld_sourcedate` `ld_sourcedate` datetime DEFAULT NULL,
  ADD COLUMN `ld_docreftype` varchar(255) DEFAULT NULL AFTER `ld_docref`,
  CHANGE COLUMN `ld_startpublishing` `ld_startpublishing` datetime DEFAULT NULL,
  CHANGE COLUMN `ld_stoppublishing` `ld_stoppublishing` datetime DEFAULT NULL,
  CHANGE COLUMN `ld_extresid` `ld_extresid` varchar(255) DEFAULT NULL,
  ADD COLUMN `ld_pages` int(11) NOT NULL,
  ADD KEY `LD_DOC_STATUS` (`ld_status`);

ALTER TABLE `ld_document_ext`
  CHANGE COLUMN `ld_datevalue` `ld_datevalue` datetime DEFAULT NULL;

ALTER TABLE `ld_extoption`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_attribute` `ld_attribute` varchar(255) NOT NULL,
  CHANGE COLUMN `ld_value` `ld_value` varchar(255) NOT NULL,
  CHANGE COLUMN `ld_label` `ld_label` varchar(1000) DEFAULT NULL;

ALTER TABLE `ld_feedmessage`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_recordversion` `ld_recordversion` bigint(20) NOT NULL AFTER `ld_lastmodified`,
  CHANGE COLUMN `ld_tenantid` `ld_tenantid` bigint(20) NOT NULL AFTER `ld_deleted`,
  CHANGE COLUMN `ld_pubdate` `ld_pubdate` datetime DEFAULT NULL;

ALTER TABLE `ld_folder`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_recordversion` `ld_recordversion` bigint(20) NOT NULL AFTER `ld_lastmodified`,
  CHANGE COLUMN `ld_tenantid` `ld_tenantid` bigint(20) NOT NULL AFTER `ld_deleted`,
  CHANGE COLUMN `ld_creation` `ld_creation` datetime DEFAULT NULL,
  ADD COLUMN `ld_position` int(11) NOT NULL;

ALTER TABLE `ld_folder`
  DROP FOREIGN KEY `FK_FOLDER_PARENT`,
  ADD CONSTRAINT `FK_FOLDER_PARENT` FOREIGN KEY (`ld_parentid`) REFERENCES `ld_folder` (`ld_id`);

ALTER TABLE `ld_folder_ext`
  CHANGE COLUMN `ld_datevalue` `ld_datevalue` datetime DEFAULT NULL;

ALTER TABLE `ld_folder_history`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_recordversion` `ld_recordversion` bigint(20) NOT NULL AFTER `ld_lastmodified`,
  CHANGE COLUMN `ld_tenantid` `ld_tenantid` bigint(20) NOT NULL AFTER `ld_deleted`,
  CHANGE COLUMN `ld_date` `ld_date` datetime DEFAULT NULL,
  CHANGE COLUMN `ld_titleold` `ld_titleold` varchar(255) DEFAULT NULL,
  CHANGE COLUMN `ld_pathold` `ld_pathold` varchar(4000) DEFAULT NULL;

ALTER TABLE `ld_foldergroup`
  ADD COLUMN `ld_subscription` int(11) NOT NULL,
  DROP PRIMARY KEY,
  ADD PRIMARY KEY (`ld_folderid`, `ld_groupid`, `ld_write`, `ld_add`, `ld_security`, `ld_immutable`, `ld_delete`, `ld_rename`, `ld_import`, `ld_export`, `ld_sign`, `ld_archive`, `ld_workflow`, `ld_download`, `ld_calendar`, `ld_subscription`);

ALTER TABLE `ld_generic`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_recordversion` `ld_recordversion` bigint(20) NOT NULL AFTER `ld_lastmodified`,
  CHANGE COLUMN `ld_tenantid` `ld_tenantid` bigint(20) NOT NULL AFTER `ld_deleted`,
  CHANGE COLUMN `ld_string3` `ld_string3` varchar(4000) DEFAULT NULL,
  CHANGE COLUMN `ld_date1` `ld_date1` datetime DEFAULT NULL,
  CHANGE COLUMN `ld_date2` `ld_date2` datetime DEFAULT NULL;

ALTER TABLE `ld_generic_ext`
  CHANGE COLUMN `ld_editor` `ld_editor` bigint(20) NOT NULL,
  CHANGE COLUMN `ld_datevalue` `ld_datevalue` datetime DEFAULT NULL;

ALTER TABLE `ld_group`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_recordversion` `ld_recordversion` bigint(20) NOT NULL AFTER `ld_lastmodified`,
  CHANGE COLUMN `ld_tenantid` `ld_tenantid` bigint(20) NOT NULL AFTER `ld_deleted`;

ALTER TABLE `ld_history`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_recordversion` `ld_recordversion` bigint(20) NOT NULL AFTER `ld_lastmodified`,
  CHANGE COLUMN `ld_tenantid` `ld_tenantid` bigint(20) NOT NULL AFTER `ld_deleted`,
  CHANGE COLUMN `ld_date` `ld_date` datetime DEFAULT NULL,
  CHANGE COLUMN `ld_titleold` `ld_titleold` varchar(255) DEFAULT NULL,
  CHANGE COLUMN `ld_pathold` `ld_pathold` varchar(4000) DEFAULT NULL;

ALTER TABLE `ld_link`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_recordversion` `ld_recordversion` bigint(20) NOT NULL AFTER `ld_lastmodified`,
  CHANGE COLUMN `ld_tenantid` `ld_tenantid` bigint(20) NOT NULL AFTER `ld_deleted`;

-- Update menu table structure but archive and add new table with actual 7.2 menu contents instead.
ALTER TABLE `ld_menu`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_recordversion` `ld_recordversion` bigint(20) NOT NULL AFTER `ld_lastmodified`,
  CHANGE COLUMN `ld_tenantid` `ld_tenantid` bigint(20) NOT NULL AFTER `ld_deleted`,
  ADD COLUMN `ld_position` int(11) NOT NULL,
  -- Drop constraint for `ld_menu` to allow us to create the new table instead.
  DROP FOREIGN KEY `FK_MENU_PARENT`;

RENAME TABLE `ld_menu` TO `ld_menu__7_1`;

CREATE TABLE `ld_menu` (
  `ld_id` bigint(20) NOT NULL,
  `ld_lastmodified` datetime NOT NULL,
  `ld_recordversion` bigint(20) NOT NULL,
  `ld_deleted` int(11) NOT NULL,
  `ld_tenantid` bigint(20) NOT NULL,
  `ld_text` varchar(255) DEFAULT NULL,
  `ld_parentid` bigint(20) NOT NULL,
  `ld_securityref` bigint(20) DEFAULT NULL,
  `ld_icon` varchar(255) DEFAULT NULL,
  `ld_type` int(11) NOT NULL,
  `ld_description` varchar(4000) DEFAULT NULL,
  `ld_position` int(11) NOT NULL,
  PRIMARY KEY (`ld_id`),
  KEY `FK_MENU_PARENT` (`ld_parentid`),
  CONSTRAINT `FK_MENU_PARENT` FOREIGN KEY (`ld_parentid`) REFERENCES `ld_menu` (`ld_id`)
) ENGINE = InnoDB DEFAULT CHARSET = `utf8`;

/*!40014 SET @`OLD_FOREIGN_KEY_CHECKS` = @@`FOREIGN_KEY_CHECKS`, FOREIGN_KEY_CHECKS = 0 */;
INSERT INTO `ld_menu` (`ld_id`, `ld_lastmodified`, `ld_recordversion`, `ld_deleted`, `ld_tenantid`, `ld_text`, `ld_parentid`, `ld_securityref`, `ld_icon`, `ld_type`, `ld_description`, `ld_position`)
VALUES
  (-2070, '2015-12-16 16:02:44', 1, 0, 1, 'dropbox', 16, NULL, 'document.png', 1, NULL, 1),
  (-5, '2015-12-16 16:02:44', 1, 0, 1, 'deleteddocs', 90, NULL, 'menu.png', 1, NULL, 1),
  (-3, '2015-12-16 16:02:44', 1, 0, 1, 'lockeddocs', 90, NULL, 'menu.png', 1, NULL, 1),
  (-2, '2015-12-16 16:02:44', 1, 0, 1, 'lastchanges', 90, NULL, 'menu.png', 1, NULL, 1),
  (1, '2015-12-16 16:02:44', 1, 0, 1, '/', 1, NULL, 'menu.png', 1, NULL, 1),
  (2, '2015-12-16 16:02:44', 1, 0, 1, 'administration', 1, NULL, 'menu.png', 1, NULL, 20),
  (3, '2015-12-16 16:02:44', 1, 0, 1, 'clienandextapps', 7, NULL, 'menu.png', 1, NULL, 1),
  (5, '2015-12-16 16:02:44', 1, 0, 1, 'frontend', 1, NULL, 'menu.png', 1, NULL, 1),
  (7, '2015-12-16 16:02:44', 1, 0, 1, 'settings', 2, NULL, 'menu.png', 1, NULL, 60),
  (8, '2015-12-16 16:02:44', 1, 0, 1, 'impex', 2, NULL, 'menu.png', 1, NULL, 40),
  (9, '2015-12-16 16:02:44', 1, 0, 1, 'security', 2, NULL, 'menu.png', 1, NULL, 20),
  (14, '2015-12-16 16:02:44', 1, 0, 1, 'scheduledtasks', 80, NULL, 'menu.png', 1, NULL, 1),
  (16, '2015-12-16 16:02:44', 1, 0, 1, 'tools', 110, NULL, 'menu.png', 1, NULL, 20),
  (25, '2015-12-16 16:02:44', 1, 0, 1, 'documentmetadata', 2, NULL, 'menu.png', 1, NULL, 30),
  (40, '2015-12-16 16:02:44', 1, 0, 1, 'personal', 110, NULL, 'menu.png', 1, NULL, 10),
  (80, '2015-12-16 16:02:44', 1, 0, 1, 'system', 2, NULL, 'system.png', 1, NULL, 10),
  (90, '2015-12-16 16:02:44', 1, 0, 1, 'reports', 2, NULL, 'reports.png', 1, NULL, 50),
  (100, '2015-12-16 16:02:44', 1, 0, 1, 'parameters', 7, NULL, 'settings.png', 1, NULL, 60),
  (110, '2015-12-16 16:02:44', 1, 0, 1, 'mainmenu', 5, NULL, 'menu.png', 1, NULL, 10),
  (1500, '2015-12-16 16:02:44', 1, 0, 1, 'documents', 5, NULL, 'menu.png', 1, NULL, 50),
  (1510, '2015-12-16 16:02:44', 1, 0, 1, 'search', 5, NULL, 'menu.png', 1, NULL, 60),
  (1520, '2015-12-16 16:02:44', 1, 0, 1, 'dashboard', 5, NULL, 'menu.png', 1, NULL, 40),
  (1530, '2015-12-16 16:02:44', 1, 0, 1, 'contacts', 40, NULL, 'menu.png', 1, NULL, 1),
  (1600, '2015-12-16 16:02:44', 1, 0, 1, 'history', 1500, NULL, 'menu.png', 1, NULL, 10);
/*!40014 SET FOREIGN_KEY_CHECKS = @`OLD_FOREIGN_KEY_CHECKS` */;

UPDATE `hibernate_unique_key`
  SET `next_hi` = 5000
  WHERE `tablename` = 'ld_menu' LIMIT 1;

-- No changes to `ld_menugroup`.
-- Also archive and add new table with actual 7.2 menu groups instead.
ALTER TABLE `ld_menugroup`
  DROP FOREIGN KEY `FKB4F7F679AA456AD1`,
  DROP FOREIGN KEY `FK_MENUGROUP_GROUP`;

RENAME TABLE `ld_menugroup` TO `ld_menugroup__7_1`;

CREATE TABLE `ld_menugroup` (
  `ld_menuid` bigint(20) NOT NULL,
  `ld_groupid` bigint(20) NOT NULL,
  `ld_write` int(11) NOT NULL,
  PRIMARY KEY (`ld_menuid`, `ld_groupid`, `ld_write`),
  KEY `FK_MENUGROUP_GROUP` (`ld_groupid`),
  CONSTRAINT `FKB4F7F679AA456AD1` FOREIGN KEY (`ld_menuid`) REFERENCES `ld_menu` (`ld_id`),
  CONSTRAINT `FK_MENUGROUP_GROUP` FOREIGN KEY (`ld_groupid`) REFERENCES `ld_group` (`ld_id`) ON DELETE CASCADE
) ENGINE = InnoDB DEFAULT CHARSET = `utf8`;

/*!40014 SET @`OLD_FOREIGN_KEY_CHECKS` = @@`FOREIGN_KEY_CHECKS`, FOREIGN_KEY_CHECKS = 0 */;
INSERT INTO `ld_menugroup` (`ld_menuid`, `ld_groupid`, `ld_write`)
VALUES
  (-2070, -10000, 0),
  (-2070, 2, 0),
  (-2070, 3, 0),
  (-2070, 4, 0),
  (2, 4, 0),
  (5, -10000, 0),
  (5, 2, 0),
  (5, 3, 0),
  (5, 4, 0),
  (14, 4, 0),
  (25, 4, 0),
  (1500, -10000, 0),
  (1500, 2, 0),
  (1500, 3, 0),
  (1500, 4, 0),
  (1510, -10000, 0),
  (1510, 2, 0),
  (1510, 3, 0),
  (1510, 4, 0),
  (1520, -10000, 0),
  (1520, 2, 0),
  (1520, 3, 0),
  (1520, 4, 0),
  (1530, -10000, 0),
  (1530, 2, 0),
  (1530, 3, 0),
  (1530, 4, 0);
/*!40014 SET FOREIGN_KEY_CHECKS = @`OLD_FOREIGN_KEY_CHECKS` */;

ALTER TABLE `ld_messagetemplate`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_recordversion` `ld_recordversion` bigint(20) NOT NULL AFTER `ld_lastmodified`,
  CHANGE COLUMN `ld_tenantid` `ld_tenantid` bigint(20) NOT NULL AFTER `ld_deleted`;

UPDATE `ld_messagetemplate`
  SET `ld_subject` = '$_product - Audit'
  WHERE `ld_name` = 'audit';

ALTER TABLE `ld_note`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_recordversion` `ld_recordversion` bigint(20) NOT NULL AFTER `ld_lastmodified`,
  CHANGE COLUMN `ld_tenantid` `ld_tenantid` bigint(20) NOT NULL AFTER `ld_deleted`,
  CHANGE COLUMN `ld_date` `ld_date` datetime DEFAULT NULL,
  ADD COLUMN `ld_snippet` varchar(4000) DEFAULT NULL,
  ADD COLUMN `ld_page` int(11) NOT NULL;

ALTER TABLE `ld_rating`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_recordversion` `ld_recordversion` bigint(20) NOT NULL AFTER `ld_lastmodified`,
  CHANGE COLUMN `ld_tenantid` `ld_tenantid` bigint(20) NOT NULL AFTER `ld_deleted`;

-- No changes to `ld_recipient`.

ALTER TABLE `ld_sequence`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) NOT NULL,
  CHANGE COLUMN `ld_lastreset` `ld_lastreset` datetime DEFAULT NULL;

ALTER TABLE `ld_systemmessage`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_recordversion` `ld_recordversion` bigint(20) NOT NULL AFTER `ld_lastmodified`,
  CHANGE COLUMN `ld_tenantid` `ld_tenantid` bigint(20) NOT NULL AFTER `ld_deleted`,
  CHANGE COLUMN `ld_messagetext` `ld_messagetext` varchar(4000) DEFAULT NULL,
  CHANGE COLUMN `ld_sentdate` `ld_sentdate` datetime NOT NULL,
  CHANGE COLUMN `ld_lastnotified` `ld_lastnotified` datetime DEFAULT NULL;

ALTER TABLE `ld_tag`
  ADD COLUMN `ld_tenantid` bigint(20) NOT NULL AFTER `ld_docid`;

ALTER TABLE `ld_template`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_recordversion` `ld_recordversion` bigint(20) NOT NULL AFTER `ld_lastmodified`,
  CHANGE COLUMN `ld_tenantid` `ld_tenantid` bigint(20) NOT NULL AFTER `ld_deleted`,
  DROP COLUMN `ld_retentiondays`;

ALTER TABLE `ld_template_ext`
  CHANGE COLUMN `ld_datevalue` `ld_datevalue` datetime DEFAULT NULL;

ALTER TABLE `ld_tenant`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) DEFAULT NULL,
  CHANGE COLUMN `ld_displayname` `ld_displayname` varchar(4000) DEFAULT NULL,
  CHANGE COLUMN `ld_expire` `ld_expire` datetime DEFAULT NULL,
  CHANGE COLUMN `ld_street` `ld_street` varchar(255) DEFAULT NULL,
  CHANGE COLUMN `ld_postalcode` `ld_postalcode` varchar(255) DEFAULT NULL,
  CHANGE COLUMN `ld_city` `ld_city` varchar(255) DEFAULT NULL,
  CHANGE COLUMN `ld_country` `ld_country` varchar(255) DEFAULT NULL,
  CHANGE COLUMN `ld_state` `ld_state` varchar(255) DEFAULT NULL,
  CHANGE COLUMN `ld_email` `ld_email` varchar(255) DEFAULT NULL,
  CHANGE COLUMN `ld_telephone` `ld_telephone` varchar(255) DEFAULT NULL;

ALTER TABLE `ld_ticket`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_recordversion` `ld_recordversion` bigint(20) NOT NULL AFTER `ld_lastmodified`,
  CHANGE COLUMN `ld_tenantid` `ld_tenantid` bigint(20) NOT NULL AFTER `ld_deleted`,
  CHANGE COLUMN `ld_creation` `ld_creation` datetime NOT NULL,
  CHANGE COLUMN `ld_expired` `ld_expired` datetime DEFAULT NULL;

-- Signatures were unused. Not sure if there is a proper conversion available
-- otherwise. Proceed with care.
ALTER TABLE `ld_user`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_recordversion` `ld_recordversion` bigint(20) NOT NULL AFTER `ld_lastmodified`,
  CHANGE COLUMN `ld_tenantid` `ld_tenantid` bigint(20) NOT NULL AFTER `ld_deleted`,
  CHANGE COLUMN `ld_passwordchanged` `ld_passwordchanged` datetime DEFAULT NULL,
  ADD COLUMN `ld_keydigest` varchar(255) DEFAULT NULL AFTER `ld_quota`,
  ADD COLUMN `ld_key` varchar(4000) DEFAULT NULL AFTER `ld_quota`,
  ADD COLUMN `ld_certdigest` varchar(255) DEFAULT NULL AFTER `ld_quota`,
  ADD COLUMN `ld_certsubject` varchar(255) DEFAULT NULL AFTER `ld_quota`,
  ADD COLUMN `ld_cert` varchar(4000) DEFAULT NULL AFTER `ld_quota`,
  DROP COLUMN `ld_signatureid`,
  DROP COLUMN `ld_signatureinfo`;

ALTER TABLE `ld_user_history`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_recordversion` `ld_recordversion` bigint(20) NOT NULL AFTER `ld_lastmodified`,
  CHANGE COLUMN `ld_tenantid` `ld_tenantid` bigint(20) NOT NULL AFTER `ld_deleted`,
  CHANGE COLUMN `ld_date` `ld_date` datetime DEFAULT NULL;

-- No changes to `ld_usergroup`.

ALTER TABLE `ld_version`
  CHANGE COLUMN `ld_lastmodified` `ld_lastmodified` datetime NOT NULL,
  CHANGE COLUMN `ld_recordversion` `ld_recordversion` bigint(20) NOT NULL AFTER `ld_lastmodified`,
  CHANGE COLUMN `ld_tenantid` `ld_tenantid` bigint(20) NOT NULL AFTER `ld_deleted`,
  CHANGE COLUMN `ld_customid` `ld_customid` varchar(200) DEFAULT NULL,
  CHANGE COLUMN `ld_date` `ld_date` datetime DEFAULT NULL,
  CHANGE COLUMN `ld_creation` `ld_creation` datetime DEFAULT NULL,
  CHANGE COLUMN `ld_sourcedate` `ld_sourcedate` datetime DEFAULT NULL,
  CHANGE COLUMN `ld_versiondate` `ld_versiondate` datetime DEFAULT NULL,
  CHANGE COLUMN `ld_startpublishing` `ld_startpublishing` datetime DEFAULT NULL,
  CHANGE COLUMN `ld_stoppublishing` `ld_stoppublishing` datetime DEFAULT NULL,
  ADD COLUMN `ld_pages` int(11) NOT NULL;

ALTER TABLE `ld_version_ext`
  CHANGE COLUMN `ld_datevalue` `ld_datevalue` datetime DEFAULT NULL;

/*!40111 SET SQL_NOTES = @`OLD_SQL_NOTES` */;
/*!40101 SET SQL_MODE = @`OLD_SQL_MODE` */;