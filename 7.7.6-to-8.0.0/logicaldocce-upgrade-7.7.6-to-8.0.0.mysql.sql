-- Unofficial LogicalDOC CE 7.7.6 to 8.0.0 database schema migration script by
-- Michael Maier <michael.maier@xplo.re>

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
-- IN THE SOFTWARE.


-- Explicit switch to UTF-8 binary.
ALTER TABLE `ld_user`
  CHANGE COLUMN `ld_ipblacklist` `ld_ipblacklist` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_password` `ld_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_ipwhitelist` `ld_ipwhitelist` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_passwordmd4` `ld_passwordmd4` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL;


-- Update default message templates.
UPDATE `ld_messagetemplate` SET `ld_type` = "system" WHERE `ld_id` IN (1, 2, 3, 4);
UPDATE `ld_messagetemplate`
  SET
    `ld_lastmodified` = NOW(),
    `ld_body` = '$I18N.format(\'emailnotifyaccount\', $user.fullName) <br/>\r\n$I18N.get(\'username\'): <b>$user.username</b> <br/>\r\n$I18N.get(\'password\'): <b>$password</b> <br/>\r\n$I18N.get(\'clickhere\'): <a href=\"$url\">$url</a>'
  WHERE `ld_id` = 2;

-- New default system user.
-- Old workflow user -1050 may be obsolete and removed if not referenced anywhere.
INSERT INTO `ld_user` (`ld_id`, `ld_lastmodified`, `ld_recordversion`, `ld_deleted`, `ld_tenantid`, `ld_enabled`, `ld_username`, `ld_password`, `ld_passwordmd4`, `ld_name`, `ld_firstname`, `ld_street`, `ld_postalcode`, `ld_city`, `ld_country`, `ld_state`, `ld_language`, `ld_email`, `ld_emailsignature`, `ld_telephone`, `ld_telephone2`, `ld_type`, `ld_passwordchanged`, `ld_passwordexpires`, `ld_source`, `ld_quota`, `ld_welcomescreen`, `ld_ipwhitelist`, `ld_ipblacklist`, `ld_passwordexpired`, `ld_defworkspace`, `ld_email2`, `ld_emailsignature2`, `ld_certexpire`, `ld_certdn`, `ld_certpass`, `ld_secondfactor`, `ld_key`)
VALUES
  (-1010, NOW(), 1, 0, 1, 1, '_system', '', NULL, 'User', 'System', '', '', '', '', NULL, 'en', 'system@acme.com', NULL, '', NULL, 1, NULL, 0, 0, -1, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO `ld_group` (`ld_id`, `ld_lastmodified`, `ld_recordversion`, `ld_deleted`, `ld_tenantid`, `ld_name`, `ld_description`, `ld_type`)
VALUES
  (-1010, NOW(), 1, 0, 1, '_user_-1010', '', 1);

INSERT INTO `ld_usergroup` (`ld_groupid`, `ld_userid`)
VALUES
  (-1010, -1010);