-- Unofficial LogicalDOC CE 8.0.0 to 8.1.1 database schema migration script by
-- Michael Maier <michael.maier@xplo.re>

-- ATTENTION!
-- The official 8.1.1 installer explicitly sets the character set UTF-8 and
-- collation utf8_bin for every text column. This upgrade script follows the
-- default 8.1.1 configuration.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
-- IN THE SOFTWARE.


-- The official install script explicitly uses utf8_bin for all text columns.
-- Update existing text columns.

-- ATTENTION: Update your database name.
ALTER DATABASE `logicaldoc`
  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `hibernate_sequences`
  CHANGE COLUMN `sequence_next_hi_value` `next_val` bigint(20) NOT NULL,
  CHANGE COLUMN `sequence_name` `sequence_name` varchar(40) COLLATE utf8_bin NOT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_attributeset`
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) COLLATE utf8_bin NOT NULL,
  CHANGE COLUMN `ld_description` `ld_description` varchar(2000) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_attributeset_ext`
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) COLLATE utf8_bin NOT NULL,
  CHANGE COLUMN `ld_label` `ld_label` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_stringvalue` `ld_stringvalue` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_bookmark`
  CHANGE COLUMN `ld_filetype` `ld_filetype` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_title` `ld_title` varchar(255) COLLATE utf8_bin NOT NULL,
  CHANGE COLUMN `ld_description` `ld_description` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_contact`
  CHANGE COLUMN `ld_email` `ld_email` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_phone` `ld_phone` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_firstname` `ld_firstname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_mobile` `ld_mobile` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_lastname` `ld_lastname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_address` `ld_address` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_company` `ld_company` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_document`
  CHANGE COLUMN `ld_exportname` `ld_exportname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_version` `ld_version` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_docreftype` `ld_docreftype` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_customid` `ld_customid` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_exportversion` `ld_exportversion` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_password` `ld_password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_transactionid` `ld_transactionid` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_comment` `ld_comment` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_workflowstatusdisp` `ld_workflowstatusdisp` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_type` `ld_type` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_filename` `ld_filename` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_lockuser` `ld_lockuser` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_workflowstatus` `ld_workflowstatus` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_language` `ld_language` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_fileversion` `ld_fileversion` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_extresid` `ld_extresid` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_publisher` `ld_publisher` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_tgs` `ld_tgs` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_digest` `ld_digest` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_creator` `ld_creator` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_document_ext`
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) COLLATE utf8_bin NOT NULL,
  CHANGE COLUMN `ld_label` `ld_label` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_stringvalue` `ld_stringvalue` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_extoption`
  CHANGE COLUMN `ld_value` `ld_value` varchar(255) COLLATE utf8_bin NOT NULL,
  CHANGE COLUMN `ld_attribute` `ld_attribute` varchar(255) COLLATE utf8_bin NOT NULL,
  CHANGE COLUMN `ld_label` `ld_label` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_feedmessage`
  CHANGE COLUMN `ld_title` `ld_title` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_link` `ld_link` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_description` `ld_description` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_guid` `ld_guid` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_folder`
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_color` `ld_color` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_qrecipients` `ld_qrecipients` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_description` `ld_description` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_creator` `ld_creator` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_tgs` `ld_tgs` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_folder_ext`
  CHANGE COLUMN `ld_stringvalue` `ld_stringvalue` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_label` `ld_label` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) COLLATE utf8_bin NOT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_folder_history`
  CHANGE COLUMN `ld_username` `ld_username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_version` `ld_version` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_sessionid` `ld_sessionid` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_filenameold` `ld_filenameold` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_pathold` `ld_pathold` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_path` `ld_path` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_ip` `ld_ip` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_userlogin` `ld_userlogin` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_comment` `ld_comment` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_event` `ld_event` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_filename` `ld_filename` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_foldergroup`
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_foldertag`
  CHANGE COLUMN `ld_tag` `ld_tag` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_generic`
  CHANGE COLUMN `ld_type` `ld_type` varchar(255) COLLATE utf8_bin NOT NULL,
  CHANGE COLUMN `ld_string2` `ld_string2` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_string3` `ld_string3` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_subtype` `ld_subtype` varchar(255) COLLATE utf8_bin NOT NULL,
  CHANGE COLUMN `ld_string1` `ld_string1` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_generic_ext`
  CHANGE COLUMN `ld_stringvalue` `ld_stringvalue` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) COLLATE utf8_bin NOT NULL,
  CHANGE COLUMN `ld_label` `ld_label` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_group`
  CHANGE COLUMN `ld_description` `ld_description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) COLLATE utf8_bin NOT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_history`
  CHANGE COLUMN `ld_path` `ld_path` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_filenameold` `ld_filenameold` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_pathold` `ld_pathold` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_sessionid` `ld_sessionid` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_ip` `ld_ip` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_userlogin` `ld_userlogin` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_version` `ld_version` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_username` `ld_username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_filename` `ld_filename` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_comment` `ld_comment` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_event` `ld_event` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_link`
  CHANGE COLUMN `ld_type` `ld_type` varchar(255) COLLATE utf8_bin NOT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_menu`
  CHANGE COLUMN `ld_icon` `ld_icon` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_description` `ld_description` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_menugroup`
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_messagetemplate`
  CHANGE COLUMN `ld_description` `ld_description` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_language` `ld_language` varchar(10) COLLATE utf8_bin NOT NULL,
  CHANGE COLUMN `ld_subject` `ld_subject` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) COLLATE utf8_bin NOT NULL,
  CHANGE COLUMN `ld_type` `ld_type` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_body` `ld_body` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_note`
  CHANGE COLUMN `ld_snippet` `ld_snippet` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_message` `ld_message` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_username` `ld_username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_rating`
  CHANGE COLUMN `ld_username` `ld_username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_recipient`
  CHANGE COLUMN `ld_mode` `ld_mode` varchar(255) COLLATE utf8_bin NOT NULL,
  CHANGE COLUMN `ld_address` `ld_address` varchar(255) COLLATE utf8_bin NOT NULL,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) COLLATE utf8_bin NOT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_sequence`
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) COLLATE utf8_bin NOT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_session`
  CHANGE COLUMN `ld_clientid` `ld_clientid` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_clienthost` `ld_clienthost` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_key` `ld_key` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_node` `ld_node` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_sid` `ld_sid` varchar(255) COLLATE utf8_bin NOT NULL,
  CHANGE COLUMN `ld_tenantname` `ld_tenantname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_clientaddr` `ld_clientaddr` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_username` `ld_username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_systemmessage`
  CHANGE COLUMN `ld_messagetext` `ld_messagetext` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_author` `ld_author` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_subject` `ld_subject` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_tag`
  CHANGE COLUMN `ld_tag` `ld_tag` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_temp`
  CHANGE COLUMN `ld_string` `ld_string` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_template`
  CHANGE COLUMN `ld_description` `ld_description` varchar(2000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) COLLATE utf8_bin NOT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_template_ext`
  CHANGE COLUMN `ld_label` `ld_label` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) COLLATE utf8_bin NOT NULL,
  CHANGE COLUMN `ld_stringvalue` `ld_stringvalue` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_tenant`
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_postalcode` `ld_postalcode` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_telephone` `ld_telephone` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_state` `ld_state` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_city` `ld_city` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_qrecipients` `ld_qrecipients` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_country` `ld_country` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_email` `ld_email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_street` `ld_street` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_displayname` `ld_displayname` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_ticket`
  CHANGE COLUMN `ld_suffix` `ld_suffix` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_ticketid` `ld_ticketid` varchar(255) COLLATE utf8_bin NOT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_uniquetag`
  CHANGE COLUMN `ld_tag` `ld_tag` varchar(255) COLLATE utf8_bin NOT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_update`
  CHANGE COLUMN `ld_update` `ld_update` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_version` `ld_version` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_user`
  CHANGE COLUMN `ld_telephone` `ld_telephone` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_postalcode` `ld_postalcode` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_username` `ld_username` varchar(255) COLLATE utf8_bin NOT NULL,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_country` `ld_country` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_city` `ld_city` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_language` `ld_language` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_secondfactor` `ld_secondfactor` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_ipblacklist` `ld_ipblacklist` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_password` `ld_password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_state` `ld_state` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_emailsignature2` `ld_emailsignature2` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_street` `ld_street` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_firstname` `ld_firstname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_emailsignature` `ld_emailsignature` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_email2` `ld_email2` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_certpass` `ld_certpass` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_ipwhitelist` `ld_ipwhitelist` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_email` `ld_email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_telephone2` `ld_telephone2` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_key` `ld_key` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_passwordmd4` `ld_passwordmd4` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_certdn` `ld_certdn` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_user_history`
  CHANGE COLUMN `ld_event` `ld_event` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_username` `ld_username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_comment` `ld_comment` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_filename` `ld_filename` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_ip` `ld_ip` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_userlogin` `ld_userlogin` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_sessionid` `ld_sessionid` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_usergroup`
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_version`
  CHANGE COLUMN `ld_type` `ld_type` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_workflowstatusdisp` `ld_workflowstatusdisp` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_comment` `ld_comment` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_transactionid` `ld_transactionid` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_foldername` `ld_foldername` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_filename` `ld_filename` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_version` `ld_version` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_username` `ld_username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_exportname` `ld_exportname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_password` `ld_password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_exportversion` `ld_exportversion` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_customid` `ld_customid` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_extresid` `ld_extresid` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_event` `ld_event` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_fileversion` `ld_fileversion` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_creator` `ld_creator` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_digest` `ld_digest` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_publisher` `ld_publisher` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_tgs` `ld_tgs` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_lockuser` `ld_lockuser` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_language` `ld_language` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_workflowstatus` `ld_workflowstatus` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_templatename` `ld_templatename` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `ld_version_ext`
  CHANGE COLUMN `ld_stringvalue` `ld_stringvalue` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  CHANGE COLUMN `ld_name` `ld_name` varchar(255) COLLATE utf8_bin NOT NULL,
  CHANGE COLUMN `ld_label` `ld_label` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


-- Database changes beside character set changes.
ALTER TABLE `ld_document`
  ADD COLUMN `ld_deleteuser` varchar(255) DEFAULT NULL AFTER `ld_deleteuserid`;

ALTER TABLE `ld_folder`
  ADD COLUMN `ld_deleteuser` varchar(255) DEFAULT NULL AFTER `ld_deleteuserid`,
  ADD INDEX `LD_FLD_NAME` (`ld_name`);

ALTER TABLE `ld_folder_history`
  ADD COLUMN `ld_reason` varchar(4000) DEFAULT NULL AFTER `ld_comment`;

ALTER TABLE `ld_foldergroup`
  ADD COLUMN `ld_automation` int(11) NOT NULL AFTER `ld_email`;

ALTER TABLE `ld_generic`
  ADD COLUMN `ld_string4` varchar(4000) DEFAULT NULL AFTER `ld_string3`;

ALTER TABLE `ld_history`
  ADD COLUMN `ld_reason` varchar(4000) DEFAULT NULL AFTER `ld_comment`;

ALTER TABLE `ld_recipient`
  DROP INDEX `FK406A04126621DEBE`,  -- was INDEX (`ld_messageid`)
  ADD INDEX `LD_RCP_MID_NAME` (`ld_messageid`, `ld_name`);

ALTER TABLE `ld_session`
  ADD PRIMARY KEY (`ld_id`);

ALTER TABLE `ld_user_history`
  ADD COLUMN `ld_reason` varchar(4000) DEFAULT NULL AFTER `ld_comment`,
  ADD COLUMN `ld_author` varchar(255) DEFAULT NULL AFTER `ld_ip`;


-- New menu items.
INSERT INTO `ld_menu` (`ld_id`, `ld_lastmodified`, `ld_recordversion`, `ld_deleted`, `ld_tenantid`, `ld_name`, `ld_parentid`, `ld_securityref`, `ld_icon`, `ld_type`, `ld_description`, `ld_position`)
VALUES
  (1525, NOW(), 1, 0, 1, 'messages', 1520, NULL, 'menu.png', 1, NULL, 40),
  (1603, NOW(), 1, 0, 1, 'versions', 1500, NULL, 'menu.png', 1, NULL, 10);

INSERT INTO `ld_menugroup` (`ld_menuid`, `ld_groupid`, `ld_write`)
VALUES
  (1525, -10000, 0),
  (1525, 2, 0),
  (1525, 3, 0),
  (1525, 4, 0),
  (1603, -10000, 0),
  (1603, 2, 0),
  (1603, 3, 0),
  (1603, 4, 0);