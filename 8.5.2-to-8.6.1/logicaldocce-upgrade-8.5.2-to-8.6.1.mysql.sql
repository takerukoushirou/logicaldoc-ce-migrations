-- Unofficial LogicalDOC CE 8.5.2 to 8.6.1 database schema migration script by
-- Michael Maier <michael.maier@xplo.re>

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
-- IN THE SOFTWARE.


ALTER TABLE `ld_folder_history`
  CHANGE COLUMN `ld_date` `ld_date` datetime(3) DEFAULT NULL,
  ADD COLUMN `ld_geolocation` varchar(255) DEFAULT NULL AFTER `ld_ip`,
  ADD COLUMN `ld_device` varchar(255) DEFAULT NULL AFTER `ld_geolocation`,
  ADD COLUMN `ld_filesize` bigint(20) DEFAULT NULL AFTER `ld_device`;

ALTER TABLE `ld_history`
  CHANGE COLUMN `ld_date` `ld_date` datetime(3) DEFAULT NULL,
  ADD COLUMN `ld_geolocation` varchar(255) DEFAULT NULL AFTER `ld_ip`,
  ADD COLUMN `ld_device` varchar(255) DEFAULT NULL AFTER `ld_geolocation`,
  ADD COLUMN `ld_filesize` bigint(20) DEFAULT NULL AFTER `ld_device`;

ALTER TABLE `ld_menu`
  ADD COLUMN `ld_enabled` int(11) NOT NULL AFTER `ld_position`,
  ADD COLUMN `ld_routineid` bigint(20) DEFAULT NULL AFTER `ld_enabled`,
  ADD COLUMN `ld_automation` varchar(4000) DEFAULT NULL AFTER `ld_routineid`;

ALTER TABLE `ld_note`
  ADD COLUMN `ld_shape` varchar(255) DEFAULT NULL AFTER `ld_recipientemail`,
  ADD COLUMN `ld_linecolor` varchar(255) DEFAULT NULL AFTER `ld_shape`,
  ADD COLUMN `ld_lineopacity` int(11) NOT NULL AFTER `ld_linecolor`,
  ADD COLUMN `ld_linewidth` int(11) NOT NULL AFTER `ld_lineopacity`,
  ADD COLUMN `ld_rotation` double NOT NULL AFTER `ld_linewidth`;

ALTER TABLE `ld_user`
  ADD COLUMN `ld_avatar` mediumtext DEFAULT NULL AFTER `ld_emailsignature2`;

ALTER TABLE `ld_user_history`
  CHANGE COLUMN `ld_date` `ld_date` datetime(3) DEFAULT NULL,
  ADD COLUMN `ld_geolocation` varchar(255) DEFAULT NULL AFTER `ld_ip`,
  ADD COLUMN `ld_device` varchar(255) DEFAULT NULL AFTER `ld_geolocation`,
  ADD COLUMN `ld_filesize` bigint(20) DEFAULT NULL AFTER `ld_author`;

CREATE TABLE `ld_device` (
  `ld_id` bigint(20) NOT NULL,
  `ld_lastmodified` datetime NOT NULL,
  `ld_recordversion` bigint(20) NOT NULL,
  `ld_deleted` int(11) NOT NULL,
  `ld_tenantid` bigint(20) NOT NULL,
  `ld_userid` bigint(20) DEFAULT NULL,
  `ld_deviceid` varchar(255) NOT NULL,
  `ld_creation` datetime DEFAULT NULL,
  `ld_username` varchar(255) DEFAULT NULL,
  `ld_browser` varchar(255) DEFAULT NULL,
  `ld_browserversion` varchar(255) DEFAULT NULL,
  `ld_operativesystem` varchar(255) DEFAULT NULL,
  `ld_type` varchar(255) DEFAULT NULL,
  `ld_ip` varchar(255) DEFAULT NULL,
  `ld_trusted` int(11) DEFAULT NULL,
  `ld_lastlogin` datetime DEFAULT NULL,
  PRIMARY KEY (`ld_id`),
  UNIQUE KEY `AK_DEVICE` (`ld_deviceid`),
  KEY `LD_DEV_USERID` (`ld_userid`)
) ENGINE=InnoDB;

CREATE TABLE `ld_password_history` (
  `ld_id` bigint(20) NOT NULL,
  `ld_lastmodified` datetime NOT NULL,
  `ld_recordversion` bigint(20) NOT NULL,
  `ld_deleted` int(11) NOT NULL,
  `ld_tenantid` bigint(20) NOT NULL,
  `ld_userid` bigint(20) NOT NULL,
  `ld_password` varchar(255) DEFAULT NULL,
  `ld_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ld_id`),
  KEY `LD_PHIST_USERID` (`ld_userid`)
) ENGINE=InnoDB;

CREATE TABLE `ld_search` (
  `ld_id` bigint(20) NOT NULL,
  `ld_lastmodified` datetime NOT NULL,
  `ld_recordversion` bigint(20) NOT NULL,
  `ld_deleted` int(11) NOT NULL,
  `ld_tenantid` bigint(20) NOT NULL,
  `ld_userid` bigint(20) NOT NULL,
  `ld_name` varchar(255) DEFAULT NULL,
  `ld_description` varchar(1000) DEFAULT NULL,
  `ld_options` varchar(4000) DEFAULT NULL,
  `ld_date` datetime DEFAULT NULL,
  `ld_type` int(11) NOT NULL,
  PRIMARY KEY (`ld_id`),
  UNIQUE KEY `AK_SEARCH` (`ld_userid`, `ld_name`)
) ENGINE=InnoDB;


-- New hibernate sequences.
INSERT INTO `hibernate_sequences` (`sequence_name`, `next_val`)
VALUES
  ('ld_device', 100),
  ('ld_password_history', 100),
  ('ld_search', 100);

-- Default all menu items to enabled.
UPDATE `ld_menu` SET `ld_enabled` = 1;

-- New menu items and updates to existing.
INSERT INTO `ld_menu` (`ld_id`, `ld_lastmodified`, `ld_recordversion`, `ld_deleted`, `ld_tenantid`, `ld_name`, `ld_parentid`, `ld_securityref`, `ld_icon`, `ld_type`, `ld_description`, `ld_position`, `ld_enabled`, `ld_routineid`, `ld_automation`)
VALUES
  (1511, NOW(), 1, 0, 1, 'sharesearch', 1510, NULL, 'menu.png', 1, NULL, 10, 1, NULL, NULL);

UPDATE `ld_menu` SET `ld_name` = "account" WHERE `ld_id` = 40;
UPDATE `ld_menu` SET `ld_name` = "capture" WHERE `ld_id` = 1608;

INSERT INTO `ld_menugroup` (`ld_menuid`, `ld_groupid`, `ld_write`)
VALUES
  (40, -10000, 0),
  (40, 2, 0),
  (40, 3, 0),
  (40, 4, 0);

-- Add and update default message templates.
UPDATE `ld_messagetemplate`
  SET
    `ld_lastmodified` = NOW(),
    `ld_body` = '$I18N.get(\'newdocscreated\')<br/>\r\n<hr/>\r\n$message\r\n<hr/>\r\n$I18N.get(\'user\'): <b>$creator.fullName</b> $UserTool.getAvatarImg($creator.id, 16) <br/>\r\n$I18N.get(\'date\'): <b>$DateTool.format($CURRENT_DATE, true)</b><br/>\r\n<hr/>\r\n<b>$I18N.get(\'documents\')</b>:\r\n#foreach( $doc in $documents )\r\n  <br/>$doc.fileName | <a href=\"$DocTool.downloadUrl($doc)\">$I18N.get(\'download\')</a> | <a href=\"$DocTool.displayUrl($doc)\">$I18N.get(\'display\')</a>\r\n#end'
  WHERE `ld_id` = 4;

INSERT INTO `ld_messagetemplate` (`ld_id`, `ld_lastmodified`, `ld_recordversion`, `ld_deleted`, `ld_tenantid`, `ld_name`, `ld_language`, `ld_description`, `ld_body`, `ld_type`, `ld_subject`)
VALUES
  (7, '2021-08-26 11:48:55', 1, 0, 1, 'newdevice', 'en', NULL, '$I18N.get(\'wenoticednewlogin\') <b>$user.username</b> $I18N.get(\'fromnewdevice\')<br/>\r\n$I18N.get(\'device\'): $device.browser $I18N.get(\'on\') $device.operativeSystem<br/>\r\n$I18N.get(\'dateandtime\'): $DateTool.formatDate($event.date)<br/>\r\n$I18N.get(\'ipaddress\'): $client.address<br/>\r\n#if($client.geolocation)\r\n$I18N.get(\'ipgeolocation\'): $client.geolocation<br/>\r\n#end\r\n<br/>\r\n<b>$I18N.get(\'ifthiswasyou\')</b><br/>$I18N.get(\'youcanignore\')\r\n<br/><br/>\r\n<b>$I18N.get(\'ifthiswasnotyou\')</b><br/>$I18N.get(\'stepstoprotect\')<ul>\r\n<li>$I18N.get(\'changeyourpasswd\')</li>\r\n<li>$I18N.get(\'reporttosysadmin\')</li></ul>', 'system', '$I18N.get(\'newloginto\') $product $I18N.get(\'ffrom\') $device.browser $I18N.get(\'on\') $device.operativeSystem');